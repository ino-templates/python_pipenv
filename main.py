# Standard Library
import sys

# First Party Library
from src.mymath import MyMath

if __name__ == "__main__":
    args = sys.argv
    n = int(args[1])
    m = MyMath()
    print(m.is_prime(n))
