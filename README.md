# Python w/ Pipenv

## Env

- Python = "^3.9"
- Pipenv = "^version 2021.5.29"

## Packages

### VSCode Extensions

- Python = "v2021.9.1246542782"
- Pylance = "v2021.9.2"

## Setup

```shell-session
$ mkdir myApp && cd myApp
$ pipenv --python 3.9
$ pipenv install --dev
$ pipenv install --dev black --pre # if raise lock error
$ pipenv sync --dev # if install from lock file
$ pipenv install -r ./requirements.txt # if install from requiremets.txt
```
